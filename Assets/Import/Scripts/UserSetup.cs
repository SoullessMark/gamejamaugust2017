﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserSetup : MonoBehaviour
{
    private bool isHighlighted = false;
    public Animator Cursor;
    public GameObject empty;
    public GameObject Monster;
    
    private bool emptyRaycastTimerActivated = false;
    private bool raycastIsEmpty = false;
    private Vector3 initialPos;
    private Vector3 initialDir;
    private float timer = 0;
    private bool isRunning = false;
    // Use this for initialization
    void Start()
    {
        Input.gyro.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Application.isMobilePlatform) {
            Quaternion cameraRotation = new Quaternion(Input.gyro.attitude.x, Input.gyro.attitude.y, -Input.gyro.attitude.z, -Input.gyro.attitude.w);
            transform.rotation = cameraRotation;
        }
        
        Vector3 headPosition = Camera.main.transform.position;
        Vector3 gazeDirection = Camera.main.transform.forward;

        RaycastHit hitInfo;



        if (Physics.Raycast(headPosition, gazeDirection, out hitInfo, 10000.0f, Physics.DefaultRaycastLayers))
        {
            if (hitInfo.collider.gameObject.tag == "Interactable")
            {

                

                
            }
        }
        else
        {
           
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(headPosition, gazeDirection, out hitInfo, 10000.0f, Physics.DefaultRaycastLayers))
            {
                if(hitInfo.collider.gameObject.tag == "Interactable")
                {
                    empty.transform.position = hitInfo.point;
                    initialPos = empty.transform.position;
                    if (isRunning)
                    {
                        StopAllCoroutines();
                    }
                    
                }
            }
        }

        if (Input.GetMouseButton(0))
        {

            if (Physics.Raycast(headPosition, gazeDirection, out hitInfo, 10000.0f, Physics.DefaultRaycastLayers))
            {

                if (hitInfo.collider.gameObject.tag == "Interactable")
                {
                    GameObject obj = hitInfo.collider.gameObject;
                    Monster.transform.LookAt(empty.transform);
                    timer += Time.deltaTime;

                    if(obj.tag == "Expandable")
                    {
                        





                    }

                    if(obj.tag == "Discovery")
                    {
                        
                        

                    }

                }

            }


          
        }

        if (Input.GetMouseButtonUp(0))
        {
            float distance = GetDistance(initialPos, empty.transform.position);
            Vector3 diff = new Vector3(0, 0, 0) + Monster.transform.eulerAngles;
            empty.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 10;
            
                StartCoroutine(RotateOverTime());

        }

        if (!isHighlighted)
        {
            if (Physics.Raycast(headPosition, gazeDirection, out hitInfo, 10000.0f, Physics.DefaultRaycastLayers))
            {

                if (hitInfo.collider.gameObject.tag == "Interactable")
                {
                    Cursor.SetInteger("State", 1);
                    isHighlighted = true;
                    raycastIsEmpty = false;
                    emptyRaycastTimerActivated = false;
                }

                 

            }
        }

        if (isHighlighted)
        {
            if (!Physics.Raycast(headPosition, gazeDirection, out hitInfo, 10000.0f, Physics.DefaultRaycastLayers))
            {
                if (!emptyRaycastTimerActivated)
                {
                    emptyRaycastTimerActivated = true;
                    StartCoroutine(EmptyRaycastTimer());
                }
                Cursor.SetInteger("State", 0);
                isHighlighted = false;



            }
        }
    }

    IEnumerator ResetAnim(Animator anim, float time,int state)
    {
        yield return new WaitForSeconds(time);
        anim.SetInteger("State", state);

    }

    IEnumerator EmptyRaycastTimer()
    {
        yield return new WaitForSeconds(.75f);
        if (emptyRaycastTimerActivated)
        {
            raycastIsEmpty = true;
            if (raycastIsEmpty)
            {
               
            }
        }

    }

    IEnumerator RotateOverTime()
    {
        isRunning = true;
        float _timer = 50;
        while(isRunning)
        {
            RotateWithSpeed();
            yield return new WaitForSeconds(.03f);
            _timer -= 1;
            if(_timer < 0)
            {
                break;
            }
        }
        StartCoroutine(RotateBack());
    }

    IEnumerator RotateBack()
    {
        float tyme = 34;
        while(isRunning)
        {
            yield return new WaitForSeconds(.03f);
            tyme -= 1;
            Quaternion dir = Quaternion.Euler(0,0,0);
            
            Monster.transform.rotation = Quaternion.Slerp(Monster.transform.rotation, dir, Time.deltaTime * 5);
            if(tyme < 0)
            {
                break;
            }
        }
    }

    public void RotateWithSpeed()
    {
        
        //Monster.transform.Rotate(new Vector3(_diff.x, _diff.y, _diff.z) * (speed * 50), Space.Self);
        if(empty.transform.position.y > initialPos.y)
        {
            Monster.transform.Rotate(Vector3.up * (Time.deltaTime * 575), Space.Self);
        }
        if (Monster.transform.eulerAngles.y < initialPos.y)
        {
            Monster.transform.Rotate(Vector3.down * (Time.deltaTime * 575), Space.Self);
        }
        if (Monster.transform.eulerAngles.x < initialPos.x)
        {
            Monster.transform.Rotate(Vector3.left * (Time.deltaTime * 575), Space.Self);
        }
        if (Monster.transform.eulerAngles.x > initialPos.x)
        {
            Monster.transform.Rotate(Vector3.right * (Time.deltaTime * 575), Space.Self);
        }

    }

    public float GetDistance(Vector3 init, Vector3 target)
    {
        float dis = Mathf.Sqrt((Mathf.Pow(target.x - init.y, 2)) + (Mathf.Pow(target.y - init.y, 2)));

        return dis;
    }
   
}
